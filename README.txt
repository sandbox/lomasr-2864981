-- SUMMARY --
Comment Admin Notify helps site admin to follow comments on site through emails.

-- REQUIREMENTS --

Comments Core Module must be enabled.

-- INSTALLATION --

See the installation guide here - http://drupal.org/node/70151
