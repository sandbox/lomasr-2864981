<?php

namespace Drupal\comment_admin_notify\Form;

/**
 * @file
 * This is Comment admin notify module for email notification to admin.
 */

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Configure comment_admin_notify settings for this site.
 */
class CommentAdminNotifyAdminSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'comment_admin_notify_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function getEditableConfigNames() {
    return [
      'comment_admin_notify.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $formState, Request $request = NULL) {

    /* Get the content types names. */
    $config = \Drupal::config('comment_admin_notify.settings');
    $cn_content_types = $config->get('cn_content_types') ? $config->get('cn_content_types') : [];

    $form['cn_admin_intro'] = [
      '#markup' => $this->t("Comment Admin Settings"),
    ];
    // Applicable on content types.
    $form['cn_content_types'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Content Types'),
      '#description' => $this->t('Select the content type.'),
      '#options' => node_type_get_names(),
      '#default_value' => $cn_content_types,
    ];

    // Mail message for authenticated user.
    $form['cn_user_mailtext'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Users mail text.'),
      '#description' => $this->t('The mail text that will be send to admin.'),
      '#default_value' => $config->get('cn_user_mailtext'),
    ];
    return parent::buildForm($form, $formState);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $formState) {
    $userInputValues = $formState->getUserInput();
      ->set('cn_content_types', $userInputValues['cn_content_types'])
      ->set('cn_user_mailtext', $userInputValues['cn_user_mailtext'])
      ->save();
    parent::submitForm($form, $formState);
  }

}
