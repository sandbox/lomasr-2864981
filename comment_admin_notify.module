<?php

/**
 * @file
 * This module sends a email to site admin when a comment is added.
 */

use Drupal\user\Entity\User;
use Drupal\Core\Url;

/**
 * Implements hook_comment_insert().
 */
function comment_admin_notify_comment_insert($comment) {
  $nid = $comment->entity_id->target_id;
  $site_name = \Drupal::config('system.site')->get('name');
  $account = User::load('1');
  $email = $account->getEmail();
  $config = \Drupal::config('comment_admin_notify.settings');
  // Selected node types.
  $node_type = db_select('node', 'n')
    ->fields('n', ['type'])
    ->condition('nid', $nid)
    ->execute()
    ->fetchField();
  // Matches the node types.
  if (in_array($node_type, $config->get('cn_content_types'))) {
    $params['subject'] = t('New comment on @site_name', ['@site_name' => $site_name]);
    $params['body'] = t($config->get('cn_user_mailtext'), comment_admin_notify_mail_tokens($comment));
    comment_admin_notify_send_mail($email, $params, $nid);
  }
}

/**
 * Returns an array of variables to be replaced in mail text.
 */
function comment_admin_notify_mail_tokens($comment) {
  global $base_url;
  $node_title = db_select('node_field_data', 'nfd')
    ->fields('nfd', ['title'])
    ->condition('nid', $comment->entity_id->target_id)
    ->execute()
    ->fetchField();
  $tokens = [
    '@username' => $comment->name->value,
    '@node_title' => $node_title,
    '@comment_url' => Url::fromUri($base_url . '/node/' . $comment->entity_id->target_id, ['absolute' => TRUE, 'fragment' => 'comment-' . $comment->id()])->toString(),
    '@site' => Drupal::config('system.site')->get('name'),
    '@uri' => $base_url,
    '@uri_brief' => preg_replace('!^https?://!', '', $base_url),
  ];
  return $tokens;
}

/**
 * Sending mail.
 */
function comment_admin_notify_send_mail($email, $params, $nid) {
  $send = TRUE;
  $mailer = \Drupal::service('plugin.manager.mail');
  $langcode = \Drupal::currentUser()->getPreferredLangcode();
  $result = $mailer->mail('comment_admin_notify', 'cn_user_mailtext', $email, $langcode, $params, NULL, $send);
  if ($result['result'] !== TRUE) {
    $message = t('There was a problem sending your email notification to @email for creating node @id.', ['@email' => $email, '@id' => $nid]);
    drupal_set_message($message, 'error');
    \Drupal::logger('comment_admin_notify')->error($message);
    return;
  }

  $message = t('An email notification has been sent to @email for creating node @id.', ['@email' => $email, '@id' => $nid]);
  drupal_set_message($message);
  \Drupal::logger('comment_admin_notify')->notice($message);
}

/**
 * Implements hook_mail().
 */
function comment_admin_notify_mail($key, &$message, $params) {
  $message['subject'] = $params['subject'];
  $message['body'][] = $params['body'];
}
